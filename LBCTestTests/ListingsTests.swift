//
//  LBCTestTests.swift
//  LBCTestTests
//
//  Created by Omar Chaabouni on 01/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import XCTest
@testable import LBCTest

class ListingsTests: XCTestCase {

    let listingService = ListingServiceMock()
    let categoriesService = CategoriesServiceMock()
    
    
    func testListings() {
        let viewModel = ListingsViewModel(listingsService: listingService, categorySevice: categoriesService)
        let expectation = XCTestExpectation(description: "test ListingViewModel data")
        viewModel.requestData { listings in
            XCTAssertEqual(listings.count, 20)
            XCTAssertEqual(viewModel.listings.count, 21)
            XCTAssertEqual(viewModel.categories.count, 11)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 2.0)
    }
    
    func testSortedListings() {
        let viewModel = ListingsViewModel(listingsService: listingService, categorySevice: categoriesService)
        let expectation = XCTestExpectation(description: "test ListingViewModel sorting")
        viewModel.requestData { listings in
            let listingDates = listings.compactMap({ $0.creationDate })
            for (index, item) in listingDates.enumerated() {
                guard index + 1 < listingDates.count else {
                    expectation.fulfill()
                    return
                }
                for i in index + 1 ..< listingDates.count {
                    if listings[index].isUrgent == true || listings[i].isUrgent == true {
                        XCTAssert(true)
                    } else {
                        XCTAssertGreaterThan(item, listingDates[i])
                    }
                }
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 2.0)
    }
}
