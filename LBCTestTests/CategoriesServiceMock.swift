//
//  CategoriesServiceMock.swift
//  LBCTestTests
//
//  Created by Omar Chaabouni on 07/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

@testable import LBCTest

class CategoriesServiceMock: CategoriesService {
    
    override func getCategories(_ completion: @escaping ([LBCTest.Category]?, Error?) -> Void) {
        let bundle = Bundle(for: CategoriesServiceMock.self)
        let mockName = "categories"
        
        guard let url = bundle.url(forResource: mockName, withExtension: "json"),
            let data = try? Data(contentsOf: url),
            let categories = try? JSONDecoder().decode([LBCTest.Category].self, from: data) else {
                completion(nil, ServerError.noData)
                return
        }
        
        completion(categories, nil)
    }
}
