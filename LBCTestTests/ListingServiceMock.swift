//
//  ListingServiceMock.swift
//  LBCTestTests
//
//  Created by Omar Chaabouni on 07/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation
@testable import LBCTest

class ListingServiceMock: ListingService {
    
    override func getListings(_ completion: @escaping ([ListingItem]?, Error?) -> Void) {
        let bundle = Bundle(for: ListingServiceMock.self)
        let mockName = "listings"
        
        guard let url = bundle.url(forResource: mockName, withExtension: "json"),
            let data = try? Data(contentsOf: url),
            let listings = try? JSONDecoder().decode([ListingItem].self, from: data) else {
                completion(nil, ServerError.noData)
                return
        }
        
        completion(listings, nil)
    }
}
