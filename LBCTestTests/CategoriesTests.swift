//
//  CategoriesTests.swift
//  LBCTestTests
//
//  Created by Omar Chaabouni on 07/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation
import XCTest
@testable import LBCTest

class CategoriesTests: XCTestCase {
    
    var categories: [LBCTest.Category]!
    
    override func setUp() {
        let bundle = Bundle(for: CategoriesTests.self)
        let mockName = "categories"
        
        guard let url = bundle.url(forResource: mockName, withExtension: "json"),
            let data = try? Data(contentsOf: url),
            let categoriesList = try? JSONDecoder().decode([LBCTest.Category].self, from: data) else {
                assertionFailure("Could not fetch categories from json file")
                return
        }
        categories = categoriesList
    }
    
    func testCategoriesViewCalled() {
        let listingViewCoordinator = ListingsCoordinator(UIWindow(), navigationController: UINavigationController())
        let listingsViewModel = ListingsViewModel(listingsService: ListingService(), categorySevice: CategoriesService())
        listingsViewModel.delegate = listingViewCoordinator
        listingsViewModel.filterCategories { _ in }
        XCTAssertTrue(listingViewCoordinator.coordinators.contains(where: { $0 is CategoriesCoordinator}))
    }
    
    func testCategoriesSelection() {
        let expectation = XCTestExpectation(description: "CategoriesViewModel category selection")
        let completion: (([LBCTest.Category]) -> Void) = {
            categories in
            for (index, item) in categories.enumerated() {
                XCTAssertTrue(item.isSelected == (index == 0 || index == 4) ? true : false)
            }
            expectation.fulfill()
        }
        let viewModel = CategoriesViewModel(categories, completion)
        
        viewModel.didSelectCategory(at: 0)
        viewModel.didSelectCategory(at: 4)
        viewModel.applyCategories()
        wait(for: [expectation], timeout: 2.0)
    }
}

