//
//  CategoriesViewController.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 03/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController {
    
    private var viewModel: CategoriesViewModel!
    private var categories = [Category]()
    
    lazy private var tableView: UITableView = {
           let tableView = UITableView(frame: self.view.frame, style: .grouped)
           tableView.translatesAutoresizingMaskIntoConstraints = false
           tableView.delegate = self
           tableView.dataSource = self
           tableView.separatorStyle = .none
           tableView.rowHeight = UITableView.automaticDimension
           tableView.estimatedRowHeight = 100
           tableView.register(CategoryTableViewCell.self, forCellReuseIdentifier: CategoryTableViewCell.identifier)
           tableView.backgroundColor = ThemeManager.mainBackgroundColor
           return tableView
       }()

    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }
    
    func setViewModel(_ viewModel : CategoriesViewModel) {
        self.viewModel = viewModel
    }
    
    private func initView() {
        title = TranslationManager.categoriesTitle
        self.categories = viewModel.getCategories()
        setNavigationBarButton("black_cross", action: #selector(dismissView))
        setNavigationBarButton("save", action: #selector(applyCategories), isLeftBarButtonItem: false)
        
        view.addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }

    @objc private func dismissView() {
        viewModel.onDismiss()
    }
    
    @objc private func applyCategories() {
        viewModel.applyCategories()
        dismissView()
    }

}

// MARK: - TableView Delegate & DataSource
extension CategoriesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CategoryTableViewCell.identifier) as? CategoryTableViewCell {
            cell.configure(categories[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
}

extension CategoriesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if let cell = tableView.cellForRow(at: indexPath) as? CategoryTableViewCell {
            cell.isEnabled = !cell.isEnabled
        }
//        categories[indexPath.row].isSelected = true
        categories[indexPath.row].isSelected = categories[indexPath.row].isSelected ? false : true
//        tableView.reloadRows(at: [indexPath], with: .none)
        viewModel.didSelectCategory(at: indexPath.row)
    }
}
