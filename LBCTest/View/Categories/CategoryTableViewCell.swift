//
//  CategoryTableViewCell.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 04/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    
    static let identifier = "CategoryTableViewCell"
    private var category: Category?
    
    var isEnabled: Bool = false {
        didSet {
            selectionImageView.image = isEnabled ? UIImage(named: "check_box_active") : UIImage(named: "check_box_inactive")
            self.category?.isSelected = isEnabled
        }
    }
    
    func configure(_ category: Category?) {
        self.category = category
        self.nameLabel.text = category?.name ?? ""
        self.isEnabled = category?.isSelected ?? false
    }
    
    lazy private var selectionImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "check_box_inactive"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        return imageView
    }()
    
    lazy private var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = 8
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(selectionImageView)
        return stackView
    }()
    
    lazy private var separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        view.backgroundColor = ThemeManager.separatorColor
        return view
    }()
    
    private func initViews() {
        addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24).isActive = true
        
        addSubview(separatorView)
        separatorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        separatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24).isActive = true
        separatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24).isActive = true
        
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    
    override func prepareForReuse() {
        isEnabled = false
        configure(self.category)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
