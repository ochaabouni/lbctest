//
//  ListingDetailsViewModel.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 06/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

class ListingDetailsViewModel {
    
    private(set) var listing: ListingItem!
    
    func setListing(_ listing: ListingItem) {
        self.listing = listing
    }
    
}
