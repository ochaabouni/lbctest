//
//  ListingTableViewCell.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 03/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

class ListingTableViewCell: UITableViewCell {

    static let identifier = "DeviceTableViewCell"
    
    var listing: ListingItem? {
        didSet {
            self.configureListing(listing)
        }
    }
    
    private var isLargeScreen: Bool {
        return UIScreen.main.bounds.width > 500
    }
    
    lazy private var container: UIView = {
        let container = UIView()
        container.withShadow()
        container.backgroundColor = ThemeManager.cellBackgroundColor
        container.translatesAutoresizingMaskIntoConstraints = false
        
        return container
    }()
    
    lazy private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = isLargeScreen ? .horizontal : .vertical
        stackView.spacing = 5
        stackView.distribution = isLargeScreen ? .fillProportionally : .equalSpacing
        return stackView
    }()
    
    lazy private var urgentLabel: UIPaddedLabel = {
        let label = UIPaddedLabel(topInset: 2, bottomInset: 2, leftInset: 8, rightInset: 8)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = TranslationManager.urgentTitle.uppercased()
        label.textColor = ThemeManager.urgentTintColor
        label.layer.borderWidth = 1
        label.layer.borderColor = ThemeManager.urgentTintColor?.cgColor
        label.layer.cornerRadius = 3
        label.backgroundColor = .white
        label.clipsToBounds = true
        return label
    }()
    
    lazy private(set) var itemImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 6
        return imageView
    }()
    
    lazy private var imageHeaderContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(itemImage)
        itemImage.fit(view)
        
        view.addSubview(urgentLabel)
        urgentLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 24).isActive = true
        urgentLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        
        return view
    }()
    
    lazy private var itemTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    lazy private var categoryLabel: UIPaddedLabel = {
        let label = UIPaddedLabel(topInset: 5, bottomInset: 5, leftInset: 8, rightInset: 8)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        label.layer.cornerRadius = 15
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.backgroundColor = ThemeManager.urgentBackgroundColor
        label.textColor = .white
        return label
    }()
    
    lazy private var itemPriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 25)
        return label
    }()
    
    lazy private var descriptionStackview: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 5
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.distribution = isLargeScreen ? .fillEqually : .equalSpacing
        return stackView
    }()
    
    lazy private var titleStackview: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 10
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.addArrangedSubview(itemTitleLabel)
        stackView.addArrangedSubview(categoryLabel)
        return stackView
    }()
    
    private func initViews() {
        
        self.backgroundColor = .clear
        
        // container
        addSubview(container)
        container.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        container.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12).isActive = true
        container.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        container.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        container.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        container.layer.cornerRadius = 6
        
        container.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -8).isActive = true
        stackView.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
        
        stackView.addArrangedSubview(imageHeaderContainer)
        
        if isLargeScreen {
            imageHeaderContainer.widthAnchor.constraint(equalToConstant: 300).isActive = true
        } else {
            imageHeaderContainer.heightAnchor.constraint(equalToConstant: 300).isActive = true
        }
        
        stackView.addArrangedSubview(descriptionStackview)
        descriptionStackview.addArrangedSubview(itemPriceLabel)
        
        descriptionStackview.addArrangedSubview(titleStackview)
        if isLargeScreen {
            categoryLabel.trailingAnchor.constraint(equalTo: descriptionStackview.trailingAnchor, constant: -24).isActive = true
        }
    }
    
    private func configureListing(_ listing: ListingItem?) {
        guard let listing = listing else {
            return
        }
        self.urgentLabel.isHidden = !(listing.isUrgent ?? false)
        self.itemImage.loadImage(listing.images?.small ?? "", placeHolder: UIImage(named: "placeholder"))
        if let price = listing.price {
            self.itemPriceLabel.isHidden = false
            self.itemPriceLabel.text = price.formatted() + " €"
        } else {
            self.itemPriceLabel.isHidden = true
        }
        self.categoryLabel.text = listing.category?.name ?? ""
        self.itemTitleLabel.text = listing.title ?? ""
        
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        self.itemImage.image = UIImage(named: "placeholder")
        self.configureListing(self.listing)
    }
}
