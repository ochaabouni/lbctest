//
//  ListingsViewController.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

class ListingsViewController: UIViewController {
    
    private var viewModel: ListingsViewModel!
    private var listings = [ListingItem]()
    private var isLoadingList = false
    
    fileprivate var transitionView: UIView?
    
    lazy private var tableView: UITableView = {
        let tableView = UITableView(frame: self.view.frame, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.register(ListingTableViewCell.self, forCellReuseIdentifier: ListingTableViewCell.identifier)
        tableView.backgroundColor = ThemeManager.mainBackgroundColor
        return tableView
    }()
    
    lazy private var errorView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = ThemeManager.mainBackgroundColor
        let imageView = UIImageView(image: UIImage(named: "ghost"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        view.addSubview(imageView)
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -40).isActive = true
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        label.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 12).isActive = true
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        label.text = TranslationManager.noData
        label.numberOfLines = 0
        label.textAlignment = .center
        
        let retryButton = UIButton()
        retryButton.translatesAutoresizingMaskIntoConstraints = false
        retryButton.setTitle(TranslationManager.retry, for: .normal)
        retryButton.setTitleColor(ThemeManager.buttonTint, for: .normal)
        retryButton.addTarget(self, action: #selector(self.requestData), for: .touchUpInside)
        view.addSubview(retryButton)
        retryButton.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 12).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        return view
    }()
    
    lazy private var loadingView: UIView = {
        let view = UIView()
        view.backgroundColor = ThemeManager.mainBackgroundColor
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 100).isActive = true
        view.heightAnchor.constraint(equalToConstant: 100).isActive = true
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        view.withShadow()
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initViews()
        self.requestData()
    }
    
    func setViewModel(_ viewModel : ListingsViewModel) {
        self.viewModel = viewModel
    }
    
    @objc private func requestData() {
        self.errorView.isHidden = true
        self.displayLodingView()
        self.viewModel.requestData { listings in
            self.hideLoadingView()
            guard !listings.isEmpty else {
                DispatchQueue.main.async {
                    self.errorView.isHidden = false
                }
                return
            }
            self.listings = listings
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    private func initViews() {
        
        self.title = TranslationManager.listingsTitle
        self.view.addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        self.view.addSubview(errorView)
        errorView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        errorView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        errorView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        errorView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        setNavigationBarButton("filter", action: #selector(onCategoriesClick), isLeftBarButtonItem: false)
    }
    
    @objc private func onCategoriesClick() {
        viewModel.filterCategories { (listings) in
            DispatchQueue.main.async {
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                self.listings = listings
                self.tableView.reloadData()
            }
        }
    }
    
    private func displayLodingView() {
        guard loadingView.superview == nil else {
            return
        }
        
        view.addSubview(loadingView)
        loadingView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loadingView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    private func hideLoadingView() {
        DispatchQueue.main.async {
            self.loadingView.removeFromSuperview()
        }
    }

}

extension ListingsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: ListingTableViewCell.identifier) as? ListingTableViewCell {
            cell.listing = self.listings[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listings.count
    }
}

extension ListingsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? ListingTableViewCell {
            self.transitionView = cell.itemImage
        }
        viewModel.didSelectListing(indexPath.row)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList) {
            self.isLoadingList = true
            viewModel.fetchNextData(self.listings.count) { (list) in
                guard !list.isEmpty else {
                    self.isLoadingList = false
                    return
                }
                self.listings.append(contentsOf: list)
                self.tableView.reloadData()
                self.isLoadingList = false
            }
        }
    }
}

extension ListingsViewController: TransitionController {
    
    var transitionSourceView: UIView {
        return self.transitionView ?? UIView()
    }
    
}
