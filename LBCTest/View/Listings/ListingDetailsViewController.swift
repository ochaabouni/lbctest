//
//  ListingDetailsViewController.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 06/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

class ListingDetailsViewController: UIViewController {
    
    private var viewModel: ListingDetailsViewModel!
    
    func setViewModel(_ viewModel: ListingDetailsViewModel) {
        self.viewModel = viewModel
    }
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    lazy private var itemImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = ThemeManager.separatorColor
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height / 3).isActive = true
        return imageView
    }()
    
    lazy private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .fill
        stackView.spacing = 12
        
        return stackView
    }()
    
    lazy private var urgentLabel: UILabel = {
           let label = UIPaddedLabel(topInset: 2, bottomInset: 2, leftInset: 8, rightInset: 8)
           label.translatesAutoresizingMaskIntoConstraints = false
           label.text = TranslationManager.shortUrgentTitle.uppercased()
           label.textColor = ThemeManager.urgentTintColor
           label.layer.borderWidth = 1
           label.layer.borderColor = ThemeManager.urgentTintColor?.cgColor
           label.layer.cornerRadius = 3
           label.backgroundColor = .white
           label.clipsToBounds = true
           return label
       }()
    
    lazy private var itemTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 25)
        label.numberOfLines = 0
        return label
    }()
    
    lazy private var itemPriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    lazy private var categoryLabel: UILabel = {
        let label = UIPaddedLabel(topInset: 5, bottomInset: 5, leftInset: 8, rightInset: 8)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        label.layer.cornerRadius = 15
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.backgroundColor = ThemeManager.urgentBackgroundColor
        label.textColor = .white
        return label
    }()
    
    lazy private var informationStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 5
        stackView.distribution = .fill
        stackView.alignment = .center
        return stackView
    }()
    lazy private var bodyStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .fill
        stackView.spacing = 12
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        return stackView
    }()
    
    lazy private var dateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy private var separatorContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        let separator = UIView()
        separator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(separator)
        separator.fit(view)
        separator.backgroundColor = ThemeManager.separatorColor
        view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        return view
    }()
    
    lazy private var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = TranslationManager.listingDescriptionTitle
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    lazy private var itemDescriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    lazy private var closebutton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "close"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: 40).isActive = true
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.addTarget(self, action: #selector(self.dismissView), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        configureListing()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addDissmissalButton()
        stackView.layoutSubviews()
    }
    
    private func initViews() {
        view.backgroundColor = ThemeManager.mainBackgroundColor
        
        view.addSubview(scrollView)
        scrollView.fit(view)
        
        scrollView.addSubview(stackView)
        stackView.fit(scrollView)
        
        stackView.addArrangedSubview(itemImageView)
        itemImageView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        
        stackView.addArrangedSubview(bodyStackView)
        bodyStackView.addArrangedSubview(itemTitleLabel)
        bodyStackView.addArrangedSubview(itemPriceLabel)
        bodyStackView.addArrangedSubview(informationStackView)
        
        informationStackView.addArrangedSubview(dateLabel)
        informationStackView.addArrangedSubview(categoryLabel)
        informationStackView.addArrangedSubview(urgentLabel)
        
        bodyStackView.addArrangedSubview(separatorContainer)
        bodyStackView.addArrangedSubview(descriptionLabel)
        bodyStackView.addArrangedSubview(itemDescriptionLabel)
    }
    
    private func configureListing() {
        guard let item = viewModel.listing else {
            return
        }
        
        itemImageView.loadImage(item.images?.small ?? "", placeHolder: UIImage(named: "placeholder"))
        
        itemTitleLabel.text = item.title
        if let price = item.price {
            self.itemPriceLabel.isHidden = false
            self.itemPriceLabel.text = price.formatted() + " €"
        } else {
            self.itemPriceLabel.isHidden = true
        }
        urgentLabel.isHidden = !(item.isUrgent ?? false)
        if let category = item.category?.name {
            categoryLabel.text = category
        } else {
            categoryLabel.isHidden = true
        }
        
        dateLabel.text = item.creationDate?.toReadableDate()
        itemDescriptionLabel.text = item.description
    }
    
    @objc private func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func addDissmissalButton() {
        if closebutton.superview == nil {
            view.addSubview(closebutton)
            closebutton.topAnchor.constraint(equalTo: view.topAnchor, constant: view.safeAreaInsets.top == 0 ? 24 : view.safeAreaInsets.top).isActive = true
            closebutton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        }
    }

}

extension ListingDetailsViewController: TransitionController {
    var transitionDestionationView: UIView {
        return self.itemImageView
    }
    
    var transitionDestinationFrame: CGRect {
        return CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 3)
    }
}
