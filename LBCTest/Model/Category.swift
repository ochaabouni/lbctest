//
//  Category.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 01/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

class Category: Decodable {
    var id: Int?
    var name: String?
    var isSelected: Bool = false
    
    private enum CodingKeys: String, CodingKey {
        case id, name
    }
}
