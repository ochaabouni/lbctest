//
//  ListingItem.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 01/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

class ListingItem: Decodable {
    var id: Int?
    var categoryId: Int?
    var title: String?
    var description: String?
    var price: Double?
    var images: ListingImage?
    var creationDate: Date?
    var isUrgent: Bool?
    
    var category: Category?
    
    private enum CodingKeys: String, CodingKey {
        case id, title, price, description
        case categoryId = "category_id"
        case images = "images_url"
        case creationDate = "creation_date"
        case isUrgent = "is_urgent"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try? container.decode(Int.self, forKey: .id)
        categoryId = try? container.decode(Int.self, forKey: .categoryId)
        title = try? container.decode(String.self, forKey: .title)
        description = try? container.decode(String.self, forKey: .description)
        price = try? container.decode(Double.self, forKey: .price)
        images = try? container.decode(ListingImage.self, forKey: .images)
        creationDate = try? container.decode(String.self, forKey: .creationDate).detectedDate
        isUrgent = try? container.decode(Bool.self, forKey: .isUrgent)
    }
}
