//
//  ListingImage.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 01/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

class ListingImage: Decodable {
    var small: String?
    var thumb: String?
}
