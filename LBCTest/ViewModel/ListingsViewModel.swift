//
//  ListingsViewModel.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation


class ListingsViewModel {
    
    var delegate: ListingsCoordinatorDelegate?
    let listingsService: ListingService
    let categoriesService: CategoriesService
    private(set) var listings: [ListingItem] = []
    private(set) var categories: [Category] = []
    private var numberOfListingPerTime = 20
    
    init(listingsService: ListingService, categorySevice: CategoriesService) {
        self.listingsService = listingsService
        self.categoriesService = categorySevice
    }
    
    func requestData(_ completion: @escaping (([ListingItem]) -> Void)) {
        listingsService.getListings { (listings, error) in
            guard error == nil else {
                completion([])
                return
            }
            self.listings = listings ?? []
            self.categoriesService.getCategories { (categories, error) in
                self.categories = categories ?? []
                self.listings.forEach({ listing in
                    listing.category = self.categories.first(where: { $0.id == listing.categoryId })
                })
                self.sort(&self.listings)
                if self.listings.count < self.numberOfListingPerTime {
                    completion(self.listings)
                } else {
                    completion(Array(self.listings[0..<self.numberOfListingPerTime]))
                }
                
            }
        }
    }
    
    func fetchNextData(_ startIndex: Int, _ completion: @escaping (([ListingItem]) -> Void)) {
        let listings = getFiltredList()
        if startIndex + numberOfListingPerTime < listings.count {
            completion(Array(listings[startIndex ..< startIndex + numberOfListingPerTime]))
        } else {
            completion(Array(listings[startIndex ..< listings.count]))
        }
    }
    
    func filterCategories(_ completion: @escaping (([ListingItem]) -> Void)) {
        delegate?.onCategorySelection(self.categories, { categories in
            self.categories = categories
            self.fetchNextData(0, completion)
        })
    }
    
    func didSelectListing(_ index: Int) {
        let listing = self.getFiltredList()[index]
        delegate?.didSelectListing(listing)
    }
    
    private func sort(_ listing: inout [ListingItem]) {
        listing.sort(by: { lhs, rhs in
            guard let lhsDate = lhs.creationDate, let rhsDate = rhs.creationDate else {
                return false
            }
            return lhsDate.compare(rhsDate) == .orderedDescending
        })
        listing.sort(by: { lhs, rhs in
            return lhs.isUrgent == true && rhs.isUrgent == false
        })
    }
    
    private func getFiltredList() -> [ListingItem] {
        let filtredCategories = self.categories.filter({ $0.isSelected })
        var listings = filtredCategories.isEmpty ? self.listings :  self.listings.filter({ listing in filtredCategories.contains(where: { cat in cat.id == listing.categoryId }) })
        self.sort(&listings)
        return listings
    }
    
}
