//
//  CategoriesViewModel.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 03/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

class CategoriesViewModel {
    
    private var categories = [Category]()
    private var completionHandler: (([Category]) -> Void)
    var delegate: CategoriesCoordinatorDelegate?
    
    init(_ categories: [Category], _ handler: @escaping (([Category]) -> Void)) {
        self.categories = categories
        self.completionHandler = handler
    }
    
    func didSelectCategory(at index: Int) {
        self.categories[index].isSelected = !self.categories[index].isSelected
    }
    
    func applyCategories() {
        self.completionHandler(self.categories)
        onDismiss()
    }
    
    func onDismiss() {
        delegate?.onDismiss()
    }
    
    func getCategories() -> [Category] {
        return self.categories
    }
    
    
}
