//
//  TransitionManager.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 06/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

class ListingDetailsTransitionManager: NSObject { }


extension ListingDetailsTransitionManager: UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = (transitionContext.viewController(forKey: .from) as? UINavigationController)?.viewControllers.last as? TransitionController & UIViewController,
            let toViewController = transitionContext.viewController(forKey: .to) as? TransitionController & UIViewController else {
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            return
        }
        
        let containerView = transitionContext.containerView
        (fromViewController.transitionSourceView as? UIImageView)?.isHighlighted = false
        guard let sourceView: UIView = fromViewController.transitionSourceView.copyView() else {
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            return
        }
        sourceView.frame = fromViewController.transitionSourceView.convert(fromViewController.transitionSourceView.frame, to: nil)
        sourceView.translatesAutoresizingMaskIntoConstraints = false
        let alphaView = UIView(frame: transitionContext.finalFrame(for: toViewController))
        alphaView.backgroundColor = ThemeManager.mainBackgroundColor
        containerView.addSubview(alphaView)
        alphaView.alpha = 0.0
        sourceView.constraints.forEach({ $0.isActive = false})
        sourceView.layoutIfNeeded()
        sourceView.translatesAutoresizingMaskIntoConstraints = false
        let destinationFrame = toViewController.transitionDestinationFrame
        containerView.addSubview(sourceView)
        fromViewController.transitionSourceView.isHidden = true
        fromViewController.beginAppearanceTransition(false, animated: true)
        let duration: TimeInterval = sourceView.frame.origin.y > UIScreen.main.bounds.height / 2 ? 0.3 : 0.2
        UIView.animate(withDuration: duration, animations: {
            sourceView.frame = destinationFrame
            alphaView.alpha = 1.0
            containerView.layoutIfNeeded()
            sourceView.layoutIfNeeded()
        }) { success in
            toViewController.transitionDestionationView.isHidden = false
            fromViewController.transitionSourceView.isHidden = false
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            alphaView.removeFromSuperview()
            sourceView.removeFromSuperview()
            containerView.addSubview(toViewController.view)
            fromViewController.endAppearanceTransition()
        }
    }
    
}

extension ListingDetailsTransitionManager: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
}
