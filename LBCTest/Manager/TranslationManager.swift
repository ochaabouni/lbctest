//
//  TranslationManager.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

struct TranslationManager {
    
    // dynamic localizing
    static func localize(_ key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
    
    
    static let retry = localize("retry")
    static let noData = localize("no_data")
    
    // Listings
    static let listingsTitle = localize("listings_title")
    static let urgentTitle = localize("urgent")
    static let shortUrgentTitle = localize("short_urgent")
    
    // Categories
    static let categoriesTitle = localize("categories_title")
    
    // Listing Details
    static let listingDescriptionTitle = localize("listing_description")
}
