//
//  ThemeManager.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

struct ThemeManager {
    
    static let mainBackgroundColor = UIColor(named: "main_background")
    static let separatorColor = UIColor(named: "separator")
    static let cellBackgroundColor = UIColor(named: "cell_background")
    static let shadowColor = UIColor(named: "shadow")
    static let buttonTint = UIColor(named: "navigation_tint")
    
    static let urgentTintColor = UIColor(named: "urgent")
    static let urgentBackgroundColor = UIColor.orange
}
