//
//  ListingsCoordinator.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

protocol ListingsCoordinatorDelegate: class {
    func didSelectListing(_ listing: ListingItem)
    func onCategorySelection(_ categories: [Category], _ completion: @escaping (([Category]) -> Void))
}

class ListingsCoordinator: DeallocatableCoordinator {
    
    var terminate: (Deallocallable) -> Void = { _ in }
    
    var navigationController: UINavigationController
    
    var coordinators: [DeallocatableCoordinator] = []
    
    var window: UIWindow
    
    required init(_ window: UIWindow, navigationController: UINavigationController) {
        self.window = window
        self.navigationController = UINavigationController()
    }
    
    func start() {
        coordinators.removeAll()
        let viewController = ListingsViewController()
        let viewModel = ListingsViewModel(listingsService: ListingService(), categorySevice: CategoriesService())
        viewModel.delegate = self
        viewController.setViewModel(viewModel)
        navigationController.viewControllers = [viewController]
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    func cancel() {
        terminate(self)
    }
    
    func showCategories(_ categories: [Category], _ completion: @escaping (([Category]) -> Void)) {
        let categoriesCoordinator = CategoriesCoordinator(window, navigationController: navigationController, categories, completion)
        startCoordinator(categoriesCoordinator)
    }
    
    func showListingDetails(_ listing: ListingItem) {
        let viewModel = ListingDetailsViewModel()
        viewModel.setListing(listing)
        let viewController = ListingDetailsViewController()
        viewController.setViewModel(viewModel)
        viewController.modalPresentationStyle = .overFullScreen
        let transitionDelegate = ListingDetailsTransitionManager()
        viewController.transitioningDelegate = transitionDelegate
        self.navigationController.viewControllers.last?.present(viewController, animated: true, completion: nil)
    }
    
    func dismissPresentedView() {
        self.navigationController.dismiss(animated: true, completion: nil)
    }
    
}

extension ListingsCoordinator: ListingsCoordinatorDelegate {
    
    func didSelectListing(_ listing: ListingItem) {
        showListingDetails(listing)
    }
    
    func onCategorySelection(_ categories: [Category], _ completion: @escaping (([Category]) -> Void)) {
        self.showCategories(categories, completion)
    }
    
}
