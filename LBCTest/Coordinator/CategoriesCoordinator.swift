//
//  CategoriesCoordinator.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 07/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

class CategoriesCoordinator: DeallocatableCoordinator {
    
    var terminate: (Deallocallable) -> Void = { _ in }
    
    var navigationController: UINavigationController
    
    var coordinators: [DeallocatableCoordinator] = []
    
    var window: UIWindow
    
    private var categories: [Category]?
    private var handler: (([Category]) -> Void)?
    
    convenience init(_ window: UIWindow, navigationController: UINavigationController,
         _ categories: [Category], _ handler: @escaping (([Category]) -> Void)) {
        self.init(window, navigationController: navigationController)
        self.categories = categories
        self.handler = handler
    }
    
    required init(_ window: UIWindow, navigationController: UINavigationController) {
        self.window = window
        self.navigationController = navigationController
    }
    
    func start() {
        guard let categories = categories, let handler = handler else {
            return
        }
        
        let viewModel = CategoriesViewModel(categories, handler)
        viewModel.delegate = self
        let viewController = CategoriesViewController()
        viewController.setViewModel(viewModel)
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .overFullScreen
        self.navigationController.present(navigationController, animated: true, completion: nil)
    }
    
    func cancel() {
        terminate(self)
    }
}

extension CategoriesCoordinator: CategoriesCoordinatorDelegate {
    
    func onDismiss() {
        self.navigationController.dismiss(animated: true) {
            self.cancel()
        }
    }
}

protocol CategoriesCoordinatorDelegate {
    func onDismiss()
}
