//
//  AppCoordinator.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 01/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

class AppCoordinator: DeallocatableCoordinator {
    
    var terminate: (Deallocallable) -> Void = { _ in }
    
    var navigationController: UINavigationController
    
    var coordinators: [DeallocatableCoordinator] = []
    
    var window: UIWindow
    
    required init(_ window: UIWindow, navigationController: UINavigationController) {
        self.window = window
        self.navigationController = UINavigationController()
    }
    
    func start() {
        startCoordinator(ListingsCoordinator.self)
    }
    
    func cancel() {
        terminate(self)
    }
    
}

