//
//  AppDelegate.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 01/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

     var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        UINavigationBar.applyTheme()
        
        window = UIWindow()
        
        AppCoordinator(window!, navigationController: UINavigationController())
            .start()
        
        return true
    }


}

