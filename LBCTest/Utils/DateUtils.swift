//
//  DateUtils.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation


extension String {
    
    var detectedDates: [Date] {
        let length = self.count
        let range = NSRange(location: 0, length: length)
        return (try? NSDataDetector(types: NSTextCheckingResult.CheckingType.date.rawValue)
            .matches(in: self, range: range)
            .compactMap({ $0.date })) ?? []
    }
    
    var detectedDate: Date? {
        return detectedDates.first
    }
}

extension Date {
    func toReadableDate() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.locale = Locale(identifier: "fr-FR")
        dateFormatter.doesRelativeDateFormatting = true
        return dateFormatter.string(from: self).capitalizingFirstLetter()
    }
}
