//
//  StringUtils.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 06/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
