//
//  UIPaddedLabel.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 03/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

public class UIPaddedLabel: UILabel {
        
    public var topInset: CGFloat = 0.0
    public var bottomInset: CGFloat = 0.0
    public var leftInset: CGFloat = 0.0
    public var rightInset: CGFloat = 0.0
    
    init(topInset: CGFloat = 0.0,
         bottomInset: CGFloat = 0.0,
         leftInset: CGFloat = 0.0,
         rightInset: CGFloat = 0.0) {
        super.init(frame: .zero)
        self.topInset = topInset
        self.bottomInset = bottomInset
        self.leftInset = leftInset
        self.rightInset = rightInset
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    public override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
    
    public override func sizeToFit() {
        super.sizeThatFits(intrinsicContentSize)
    }
}
