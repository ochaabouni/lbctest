//
//  Double+Utils.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 03/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation


extension Double {
    func formatted(_ decimal: Int = 2) -> String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = decimal
        return String(formatter.string(from: number) ?? "")
    }
}
