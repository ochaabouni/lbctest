//
//  UIView+shadow.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 03/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

extension UIView {
    
    func withShadow() {
        self.layer.shadowColor = ThemeManager.shadowColor?.cgColor
        self.layer.shadowOffset = CGSize(width: -2, height: -3)
        self.layer.shadowRadius = 5.0
        self.layer.shadowOpacity = 1.0
    }
    
    func fit(_ container: UIView,
             _ top: CGFloat = 0.0,
             _ leading: CGFloat = 0.0,
             _ bottom: CGFloat = 0.0,
             _ trailing: CGFloat = 0.0) {
        self.topAnchor.constraint(equalTo: container.topAnchor, constant: top).isActive = true
        self.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: bottom).isActive = true
        self.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: leading).isActive = true
        self.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: trailing).isActive = true
        
    }
    
    func copyView() -> UIView? {
        return try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData( NSKeyedArchiver.archivedData(withRootObject: self, requiringSecureCoding: false)) as? UIView
    }
}
