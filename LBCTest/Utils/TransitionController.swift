//
//  TransitionController.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 07/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

protocol TransitionController: NSObjectProtocol {
    
    var transitionSourceView: UIView { get }
    var transitionDestionationView: UIView { get }
    var transitionDestinationFrame: CGRect { get }
    
}

extension TransitionController {
    
    var transitionSourceView: UIView { return UIView() }
    var transitionDestionationView: UIView { return UIView() }
    var transitionDestinationFrame: CGRect { return .zero }
    
}

