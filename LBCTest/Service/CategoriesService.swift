//
//  CategoriesService.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 03/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

class CategoriesService {
    
    lazy var serverManager = ServerManager.shared
    
    func getCategories(_ completion: @escaping ([Category]?, Error?) -> Void) {
        serverManager.fetchCategories { (listings, error) in
            completion(listings, error)
        }
    }
}
