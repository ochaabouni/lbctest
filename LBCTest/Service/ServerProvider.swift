//
//  ServerProvider.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

protocol ServerProvider {
    func fetchListings(_ completion: @escaping ([ListingItem]?, Error?) -> Void)
    func fetchCategories(_ completion: @escaping ([Category]?, Error?) -> Void)
}

enum ServerError: Error {
    case noData
    case malformedUrl
}
