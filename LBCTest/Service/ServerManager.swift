//
//  ServerManager.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation


class ServerManager: ServerProvider {
        
    static let shared: ServerProvider = ServerManager()
    
    private let baseUrl = "https://raw.githubusercontent.com/leboncoin/paperclip/master/"
    
    private init() {}
    
    func fetchListings(_ completion: @escaping ([ListingItem]?, Error?) -> Void) {
        guard let url = URL(string: baseUrl + "listing.json") else {
            completion(nil, ServerError.malformedUrl)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        performRequest(request) { data, error  in
            guard let data = data, let listings = try? JSONDecoder().decode([ListingItem].self, from: data) else {
                completion(nil, error ?? ServerError.noData)
                return
            }
            completion(listings, nil)
        }
    }
    
    func fetchCategories(_ completion: @escaping ([Category]?, Error?) -> Void) {
        guard let url = URL(string: baseUrl + "categories.json") else {
            completion(nil, ServerError.malformedUrl)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        performRequest(request) { data, error  in
            guard let data = data, let categories = try? JSONDecoder().decode([Category].self, from: data) else {
                completion(nil, error ?? ServerError.noData)
                return
            }
            completion(categories, nil)
        }
    }
    
    private func performRequest(_ request: URLRequest, completion: @escaping (Data?, Error?) -> Void) {
        let session = URLSession(configuration: .default)
        let dataTask = session.dataTask(with: request) { (data, _, error) in
            completion(data, error)
        }
        dataTask.resume()
    }
}
