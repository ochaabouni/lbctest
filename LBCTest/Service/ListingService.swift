//
//  ListingService.swift
//  LBCTest
//
//  Created by Omar Chaabouni on 03/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation


class ListingService {
    
    lazy var serverManager = ServerManager.shared
    
    func getListings(_ completion: @escaping ([ListingItem]?, Error?) -> Void) {
        serverManager.fetchListings { (listings, error) in
            completion(listings, error)
        }
    }
}
